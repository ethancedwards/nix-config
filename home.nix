{ config, pkgs, ... }:

{
  nixpkgs.overlays =
  let
    revAr = "9dff1a6ea169f0d6e98a9e767f8a6c57d03d9ca6";
    urlAr = "https://github.com/Artturin/nixos-related-things/archive/${revAr}.tar.gz";
    arOverlay = (import (builtins.fetchTarball urlAr));
  in [
    arOverlay

    # (import (builtins.fetchTarball {
    #   url = https://github.com/nix-community/emacs-overlay/archive/master.tar.gz;
    # }))

  ];


  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "ece";
  home.homeDirectory = "/home/ece";

  home.packages = with pkgs; [ 

  (st.overrideAttrs (oldAttrs: rec {
    src = fetchGit {
        url = "https://gitlab.com/ethancedwards/st-config";
        rev = "0eedc647ff2c4d19fdaa8c27d4ae0649e44b83e5";
        # sha256 = "0000000000000000000000000000000000000000000000000000";
    };
  }))

  (dmenu.overrideAttrs (oldAttrs: rec {
    src = fetchGit {
        url = "https://gitlab.com/ethancedwards/dmenu-config";
        rev = "9cd6fe49998b48aa1b97e8b66d8895624b0ac897";
        # sha256 = "0000000000000000000000000000000000000000000000000000";
    };
  }))

  # (slock.overrideAttrs (oldAttrs: rec {
  #   src = fetchGit {
  #       url = "https://gitlab.com/ethancedwards/dmenu-config";
  #       rev = "9cd6fe49998b48aa1b97e8b66d8895624b0ac897";
  #       # sha256 = "0000000000000000000000000000000000000000000000000000";
  #   };
  # }))

    slock
    zsh 
    file
    zathura
    nextcloud-client
    doc-repos
    exa
    cmake
    gnumake
    tree
    sxhkd
    tor-browser-bundle-bin
    bspwm
    fzf
    ripgrep
    kitty
    emacs
    tree
    ctags
    youtube-dl
    neofetch
    wget
    htop
    discord
    killall
    sqlite
    xorg.xinit
  ];

  home.file.".ghci".text = ''
    :set prompt "λ> "
  '';
   
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    extraConfig = ''
      syntax enable
      set nowrap number relativenumber
      set smartindent
      set incsearch
      set showcmd
      set tabstop=8
      set softtabstop=8
      set shiftwidth=8
      set noexpandtab

      noremap <SPACE> <Nop>
      let mapleader = " "
      nnoremap <leader>h :wincmd h<CR>
      nnoremap <leader>j :wincmd j<CR>
      nnoremap <leader>k :wincmd k<CR>
      nnoremap <leader>l :wincmd l<CR>
      nnoremap <leader>n :FZF<CR>
      nnoremap <leader>e :Explore<CR>

      map <C-n> :NERDTreeToggle<CR>

      nnoremap <leader>gc :Gcommit<CR>
      nnoremap <leader>gu :Gpush<CR>
      nnoremap <leader>gp :Gpull<CR>
      nnoremap <leader>gs :G<CR>

      let g:rooter_patterns = ['.git']

      let g:tex_flavor = 'latex'
    '';
    plugins = with pkgs.vimPlugins; [
      fzf-vim # fzf.vim
      vim-rooter
      vim-commentary
      vim-css-color
      vim-fugitive
      # vifm
      lightline-vim # lightline.vim
      vim-highlightedyank
      vim-nix
      vimtex
      nerdtree
    ];
  };

  programs.firefox = {
    enable = true;
  };

  # programs.qutebrowser = {
  #   enable = true;
  # };

  programs.tmux = {
    enable = true;
    clock24 = true;
    plugins = with pkgs.tmuxPlugins; [
      sensible
      yank
      {
        plugin = dracula;
        extraConfig = ''
          set -g @dracula-border-contrast true
          set -g @dracula-show-network false
          # set -g @dracula-show-battery true
          set -g @dracula-military-time true
          set -g @dracula-show-weather false
          set -g @dracula-show-powerline false
          set -g @dracula-show-left-icon session
          set -g @dracula-cpu-usage true
          set -g @dracula-ram-usage true
          set -g @dracula-show-powerline true
          # set -g @dracula-show-flags true
          # set -g @dracula-show-time true
          set -g @dracula-refresh-rate 5
          set -g @dracula-show-fahrenheit false
        '';
      }
    ];

    extraConfig = ''
      set -g mouse on 
      set -g prefix C-x
      bind C-x send-prefix
    '';
  };

  programs.gpg = {
    enable = true;
    settings = {
      personal-cipher-preferences = "AES256 AES192 AES";
      personal-digest-preferences = "SHA512 SHA384 SHA256";
      personal-compress-preferences = "ZLIB BZIP2 ZIP Uncompressed";
      default-preference-list = "SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed";
      cert-digest-algo = "SHA512";
      s2k-digest-algo = "SHA512";
      s2k-cipher-algo = "AES256";
      charset = "utf-8";
      fixed-list-mode = "";
      no-comments = "";
      no-emit-version = "";
      no-greeting = "";
      keyid-format = "0xlong";
      list-options = "show-uid-validity";
      verify-options = "show-uid-validity";
      with-fingerprint = "";
      require-cross-certification = "";
      no-symkey-cache = "";
      use-agent = "";
      throw-keyids = "";
      keyserver = "hkps://keyserver.ubunutu.com:443";
    };
  };

  programs.git = {
    enable = true;
    userName = "Ethan Edwards";
    userEmail = "ethancarteredwards@gmail.com";
    signing.key = "0xF93DDAFA26EF2458";
   #  signing.signByDefault = true;
    extraConfig = {
      core.editor = "nvim";
      color.ui = "auto";

      pull.rebase = false;

      # sendmail settings
      sendemail.smtpServer = "smtp.gmail.com";
      sendemail.smtpServerPort = "587";
      sendemail.smtpEncryption = "TLS";
      sendemail.smtpUser = "ethancarteredwards@gmail.com";
    };
  };

  programs.bash = {
    enable = true;
  };

  # programs.feh = {
  #   enable = true;
  # };

  # xsession = {
  #   windowManager = {
  #     i3 = {
  #       enable = true;
  #       package = pkgs.i3-gaps;
  #       config = {
  #         modifier = "Mod4";
  #         terminal = "kitty";
  #         window = {
  #           titlebar = false;
  #           border = 0;
  #         };
  #         bars = [];
  #       };
  #     };
  #   };
  # };
        
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
